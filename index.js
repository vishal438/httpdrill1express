const express = require("express");
const fs = require("fs");
const uuid = require("uuid");
const port = process.env.PORT || 3000;

const app = express();

app.get("/html", (req, res) => {
  fs.readFile("./Data/index.html", "utf-8", (err, htmlContent) => {
    if (err) {
      res.status(500).send("Internal Server Error");
    } else {
      res.status(200).type("text/html").send(htmlContent);
    }
  });
});

app.get("/json", (req, res) => {
  fs.readFile("./Data/data.json", "utf-8", (err, jsonData) => {
    if (err) {
      res.status(500).send("Internal Server Error");
    } else {
      res.status(200).json(JSON.parse(jsonData));
    }
  });
});

app.get("/uuid", (req, res) => {
  const uuidv4 = uuid.v4();
  res.status(200).json({ uuid: uuidv4 });
});

app.get("/status/:code", (req, res) => {
  const statusCode = parseInt(req.params.code);
  res.send(`Status code is ${statusCode}`);
});

app.get("/delay/:seconds", (req, res) => {
  const delayInSeconds = parseFloat(req.params.seconds);
  setTimeout(() => {
    res.status(200).json({ status: 200 });
  }, delayInSeconds * 1000);
});

app.use((req, res) => {
  res.status(404).send("Status 404 Not Found");
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}/`);
});
